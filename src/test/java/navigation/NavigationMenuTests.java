package navigation;

import com.codeborne.selenide.Condition;
import motor.MainPage;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.Selenide.closeWebDriver;
import static com.codeborne.selenide.Selenide.open;
import static motor.conf.Configuration.BASE_URL;

public class NavigationMenuTests {
    @BeforeEach
    public void goToPage() {
        open(BASE_URL);
    }

    @Test
    void goToTestDrivePageTest() {
        new MainPage().testDriveButtonClick().testDriveHeader.shouldBe(Condition.visible);
    }

    @AfterEach
    public void tearDown() {
        closeWebDriver();
    }
}
