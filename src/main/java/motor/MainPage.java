package motor;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

public class MainPage {

    private SelenideElement testDriveButton = $(By.xpath("//div/a[text()='Тест-драйв']"));

    public TestDrivePage testDriveButtonClick() {
        testDriveButton.click();
        return page(TestDrivePage.class);
    }
}
