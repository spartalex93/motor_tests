package motor.conf;

import com.codeborne.selenide.WebDriverProvider;
import io.github.bonigarcia.wdm.managers.ChromeDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import static io.github.bonigarcia.wdm.config.DriverManagerType.CHROME;

public class HeadlessChromeProvider implements WebDriverProvider {
    @Override
    public WebDriver createDriver(DesiredCapabilities capabilities) {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");
        ChromeDriverManager.getInstance(CHROME).setup();

        return new ChromeDriver(options);
    }
}

